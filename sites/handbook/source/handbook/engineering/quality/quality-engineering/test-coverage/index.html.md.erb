---
layout: handbook-page-toc
title: "Test Coverage"
description: "The Quality Department has coverage to support testing particular scenarios."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Offline environments / Airgapped GitLab QA scenario

The Quality Department has a GitLab QA scenario that supports [offline environment / air-gapped](https://docs.gitlab.com/ee/user/application_security/offline_deployments) testing.
The [scenario](https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/lib/gitlab/qa/scenario/test/instance/airgapped.rb) `Test::Instance::Airgapped` is part of [GitLab QA](https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/docs/what_tests_can_be_run.md#testinstanceairgapped)
test scenarios. The suite runs against a test environment including [Gitaly Cluster](https://docs.gitlab.com/ee/administration/gitaly/) which have been configured using `iptables` to drop traffic other than specific ports which allow our test access to the test instances.

#### Test run schedule

This is triggered on the [Nightly build](https://gitlab.com/gitlab-org/quality/pipeline-common/-/blob/master/ci/nightly/qa.gitlab-ci.yml#L181),
where a near full suite of tests for CE and EE are executed (exclusions are made for tests of product features which depend on network connectivity 
such as importing data from external sources). Results of the Nightly pipeline
can be found in the generated [Allure report](https://gitlab-qa-allure-reports.s3.amazonaws.com/nightly/master/index.html),
where test states such as failures can be filtered on.
Nightly pipelines are visible at the
[Nightly Pipelines](https://gitlab.com/gitlab-org/quality/nightly/-/pipelines?page=1&scope=all&username=gitlab-bot) page (internal only).
The offline environment / airgapped test job names are `ce:airgapped` and `ee:airgapped`.
This is one of the [pipelines monitored by the Quality Engineering team](/handbook/engineering/quality/quality-engineering/debugging-qa-test-failures/#qa-test-pipelines) as part of the 
[Quality Department pipeline triage on-call rotation](/handbook/engineering/quality/quality-engineering/oncall-rotation/).

#### Other reference guides

Secure stage has additional testing to test that analyzers can execute in an offline fashion.
More information on [secure tests](https://gitlab.com/gitlab-org/security-products/tests/common/-/blob/master/README.md#known-testing-branches)(internal only).

Otherwise for setting up an offline environment for testing, the [Getting started with an offline GitLab Installation](https://docs.gitlab.com/ee/topics/offline/quick_start_guide.html) guide can be followed.
Instructions for working with secure scanners can be found in the [Offline environments](https://docs.gitlab.com/ee/user/application_security/offline_deployments/) guide.

### GitLab Upgrades

#### GitLab QA upgrade scenarios

The Quality Department has several GitLab QA scenarios that verify different upgrade types:

1. [`Test::Omnibus::UpdateFromPrevious`](https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/docs/what_tests_can_be_run.md#testomnibusupdatefromprevious-full-image-address-current_version-majorminor)
verifies upgrade from the previous (major or minor) version to the current GitLab version ([scenario code](https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/lib/gitlab/qa/scenario/test/omnibus/update_from_previous.rb)).
  - Smoke tests run against the first version installed.
  - Smoke test run again against the last version installed.
1. [`Test::Omnibus::Upgrade`](https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/docs/what_tests_can_be_run.md#testomnibusupgrade-cefull-image-address)
checks that GitLab instance can be upgraded from CE to EE ([scenario code](https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/lib/gitlab/qa/scenario/test/omnibus/upgrade.rb)).
  - Tests run against the CE image, and the EE image.

##### Test run schedule

###### 2-hourly scheduled pipeline

1. `Test::Omnibus::UpdateFromPrevious` scenario is run within the `e2e:package-and-test-ee` child pipeline which executes from a [`gitlab-org/gitlab` scheduled pipeline every 2 hours](https://gitlab.com/gitlab-org/gitlab/-/pipeline_schedules) against GitLab `master`.
  - `e2e:package-and-test-ee` child pipeline can be found on [GitLab `master` scheduled pipelines](https://gitlab.com/gitlab-org/gitlab/-/pipelines?page=1&scope=all&username=gitlab-bot&source=schedule) page (internal only).
  - The update test job names are [`update-minor`](https://gitlab.com/gitlab-org/gitlab/-/jobs/3983747027) (update from the latest minor version) and [`update-major`](https://gitlab.com/gitlab-org/gitlab/-/jobs/3983747028) (update from the latest major version).
  - Results of these jobs can be found in [Allure report](https://gitlab-qa-allure-reports.s3.amazonaws.com/e2e-package-and-test/master/index.html), where test states such as failures can be filtered on.

###### Nightly scheduled pipeline

1. `Test::Omnibus::UpdateFromPrevious` scenario is run within the `e2e:package-and-test-nightly` child pipeline which executes from a [`gitlab-org/gitlab` scheduled nightly pipeline](https://gitlab.com/gitlab-org/gitlab/-/pipeline_schedules) against GitLab `master`.
  - `e2e:package-and-test-nightly` child pipeline can be found on [GitLab `master` scheduled pipelines](https://gitlab.com/gitlab-org/gitlab/-/pipelines?page=1&scope=all&username=gitlab-bot&source=schedule) page (internal only).
  - The update test job name is `update-ee-to-ce` (update from current EE `master` package to latest CE version).
  - Results of these jobs can be found in [Allure report](https://gitlab-qa-allure-reports.s3.amazonaws.com/e2e-package-and-test/master/index.html), where test states such as failures can be filtered on.
1. `Test::Omnibus::Upgrade` scenario is run in [`gitlab-org/quality/nightly` pipelines](https://gitlab.com/gitlab-org/quality/nightly/-/pipelines?page=1&scope=all&username=gitlab-bot).
  - The upgrade test job name is [`ce:upgrade`](https://gitlab.com/gitlab-org/quality/nightly/-/jobs/3978239322) (upgrade from `gitlab/gitlab-ce:nightly` to `gitlab/gitlab-ee:nightly`).
  - Results of the Nightly pipeline can be found in [Allure report](https://gitlab-qa-allure-reports.s3.amazonaws.com/nightly/master/index.html), where test states such as failures can be filtered on.
  - [The `gitlab-org/quality/nightly` project is being migrated to `gitlab-org/gitlab` scheduled nightly pipelines](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/198).

These pipelines are [monitored by the Quality Engineering team](/handbook/engineering/quality/quality-engineering/debugging-qa-test-failures/#qa-test-pipelines) as part of the
[Quality Department pipeline triage on-call rotation](/handbook/engineering/quality/quality-engineering/oncall-rotation/).

#### Performance environments nightly upgrades

Quality team supports test performance environments listed on [Reference Architecture](https://docs.gitlab.com/ee/administration/reference_architectures/#how-to-interpret-the-results) page.
These environments are built with GitLab Environment Toolkit and are upgraded daily or weekly depending on environment to the latest
nightly image.

Detailed process is described on [Performance and Scalability](https://docs.gitlab.com/ee/administration/reference_architectures/#how-to-interpret-the-results) page.

#### Work in progress

Quality team is working on improving GitLab upgrades coverage and this effort is
tracked in [epic#9201](https://gitlab.com/groups/gitlab-org/-/epics/9201).
