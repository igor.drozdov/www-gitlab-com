---
layout: markdown_page
title: "Category Direction - Runner Fleet"
description: "This is the Product Direction Page for the Runner Fleet product category."
canonical_path: "/direction/verify/runner_fleet/"
---

## Navigation & Settings

|                       |                               |
| -                     | -                             |
| Stage                 | [Verify](/direction/verify/)  |
| Maturity              | Complete |
| Content Last Reviewed | `2023-04-24`                  |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing anzd collaborating with you as the PM -->

Thanks for visiting this direction page on the Runner Fleet category at GitLab. This page belongs to the [Runner Group](/handbook/product/categories/#runner-group) within the Verify Stage and is maintained by [Darren Eastman](mailto:deastman@gitlab.com).

### Strategy and Themes

As outlined in the Runner Core direction [page](https://about.gitlab.com/direction/verify/runner_core/), organizations that adopt the GitLab one DevSecOps platform approach [realize](https://about.gitlab.com/resources/report-forrester-tei/) significant gains in development and delivery efficiency. And with GitLab's new [AI/ML](https://about.gitlab.com/blog/2023/04/24/ai-ml-in-devsecops-series/) feature sets, we expect an order of magnitude increase in developer efficiency and reduced time to market for software-enabled technology changes. So customers who need to self-manage GitLab must consider, plan for, and operate CI/CD computing for builds at an enormous scale.

Our **vision** is that as customers more deeply integrate AI into their development processes, they can efficiently and cost-effectively manage GitLab Runner CI/CD build agents and environments at scale on any public or private cloud platform.

While FY24's critical focus is delivering best-in-class observability for managing a runner fleet, FY25 will be about integrating automation into fleet autoscaling and management.

<!-- Describe your category. Capture the main problems to be solved in market (themes). Describe how you intend to solve these with GitLab (strategy). Provide enough context that someone unfamiliar with the details of the category can understand what is being discussed. -->

### 1 year plan
<!--
1 year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road-map". Items in this section should be linked to issues or epics that are up to date. Indicate relative priority of initiatives in this section so that the audience understands the sequence in which you intend to work on them.
 -->

The [Runner Fleet Dashboard](https://gitlab.com/gitlab-org/gitlab/-/issues/390921), a component of GitLab's observability, analytics, and feedback [FY24 product investment theme](https://about.gitlab.com/direction/#fy24-product-investment-themes), is the primary goal for FY24.

A vital component of the new Fleet Dashboard is metrics such as queue wait times,  running CI jobs, and total runner saturation. By surfacing these metrics in the UI  and then building automation that uses this data, we will significantly reduce developer wait time and time to result while optimizing infrastructure costs for the CI build environment.

#### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->

In the next three months (April to June 2023)  we are focused on the following:

**Runner Fleet Dashboard**

- [Solution validation](https://gitlab.com/gitlab-org/ux-research/-/issues/2403) of the designs for the [new Runner Fleet dashboard MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/390921).

- Refining the [new database architecture blueprint](https://gitlab.com/gitlab-org/gitlab/-/issues/387672) to enable the CI build and Runner Fleet metrics vision, and specifically deliver the MVC for the Fleet Dashboard.

- [Runner Fleet dashboard MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/390921).

#### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

**[Next Runner Token Architecture](https://docs.gitlab.com/ee/architecture/blueprints/runner_tokens/#next-gitlab-runner-token-architecture):**

Release new runner creation workflows in the following views:

- [admin](https://gitlab.com/gitlab-org/gitlab/-/issues/383139)
- [group](https://gitlab.com/gitlab-org/gitlab/-/issues/383143)
- [project](https://gitlab.com/gitlab-org/gitlab/-/issues/383144)


#### What we recently completed
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

In the past three months, we have shipped the following key features:

- [Job execution status badge in Admin Area > Runners](https://gitlab.com/gitlab-org/gitlab/-/issues/372869)
- [Job queued durations in Admin Area > Runners](https://gitlab.com/gitlab-org/gitlab/-/issues/335102/)
- [REST API to create a runner associated with a user](https://gitlab.com/gitlab-org/gitlab/-/issues/390427)

#### What is Not Planned Right Now
<!--  Often it's just as important to talk about what you're not doing as it is to discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should in fact do. We should limit this to a few items that are at a high enough level so someone with not a lot of detailed information about the product can understand -->

In the near term, we are not focused on design or development efforts to improve Runners usability in [CI/CD settings](https://docs.gitlab.com/ee/administration/#cicd-settings) at the [project level](https://gitlab.com/groups/gitlab-org/-/epics/6867). While improvements in this view could be valuable to the [software developer](/handbook/product/personas/#sasha-software-developer) persona, our primary target persona for Runner Fleet in FY24 is the [infrastructure operations](/handbook/product/personas/#ingrid-infrastructure-operator) persona.

### Best in Class Landscape
<!-- Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->

BIC (Best In Class) is an indicator of forecasted near-term market performance based on a combination of factors, including analyst views, market news, and feedback from the sales and product teams. It is critical that we understand where GitLab appears in the BIC landscape.

At GitLab, a critical challenge is simplifying the administration and management of a CI/CD build fleet at an enterprise scale. This effort is one foundational pillar to realizing the vision of AI-optimized DevOps. Competitors are also investing in this general category. Earlier this year GitHub announced a new management experience that provides a summary view of GitHub-hosted runners. This is a signal that there will be a focus on reducing maintenance and configuration overhead for managing a CI/CD build environment at scale across the industry.

We also now see additional features on the GitLab public roadmap signaling an increased investment in the category we coined here at GitLab, 'Runner Fleet'.  These features suggest that GitHub's goals are to provide a first-class experience for managing GitHub Actions runners and include features in the UI to simplify runner queue management and resolve performance bottlenecks. With this level of planned investment, it is clear that there is recognition in the market that simplifying the administrative maintenance and overhead of the CI build fleet is critical for large customers and will help enable deeper product adoption.

To ensure that our GitLab customers can fully realize the value of GitLab's product vision, we must provide solutions that eliminate the complexities, manual tasks, and operational overhead and reduce the costs of delivering a CI build environment at scale.

#### Key Capabilities
<!-- For this product area, these are the capabilities a best-in-class solution should provide -->

The key capabilities that we hear from customers describing fleet management pain points are as follows:
- Runner queue visibility
- Frictionless upgrades
- Security
- Cost visibility for runners hosted on public cloud infrastructure
- Fleet autoscaling
- Automatic fleet configuration optimization
- Managing runner sprawl
- Configuring and managing a heterogeneous runner fleet (container builds on Linux, container builds on Windows, shell builds on Windows, shell builds on macOS)
- Self-service runner creation for the developer persona
- Automating choosing the right cloud and compute to host a Runner based on CI/CD build performance

#### Roadmap
<!-- Key deliverables we're focusing on to build a BIC solution. List the epics by title and link to the epic in GitLab. Minimize additional description here so that the epics can remain the SSOT. -->

In FY 2024, the critical focus area for achieving best-in-class is delivering the new Fleet management experience, including Runner queue and performance metrics.

#### Top [1/2/3] Competitive Solutions

Runner Fleet is still a nascent category; however, competitors like GitHub are beginning to invest in this area. In Q2 2023 (APR-JUN), GitHub plans to introduce [seamless management of GitHub-hosted and self-hosted runners](https://github.com/github/roadmap/issues/504). This feature aims to deliver a "single management plane to manage all runners for a team using GitHub." GitHub also plans to offer [Actions Performance Metrics](https://github.com/github/roadmap/issues/561) to provide organizations with deep insights into critical CI/CD performance metrics. It will be soon that GitHub brings to market AI-based solutions that integrate GitHub Actions with infrastructure on Azure. While that may be compelling, with GitLab Runners, organizations can choose any cloud provider.

### Maturity Plan

- The Runner Fleet [category maturity scorecard](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1995) project ended on 2022-09-14. Runner Fleet scored 3.63, which puts the maturity level at ["Complete"](/direction/maturity/).
- Our next maturity target is Lovable, 2024-01-22, see our [definitions of maturity levels](/direction/maturity/) and depends on delvering the FY24 roadmap.
